#!/usr/bin/env bash


VBR="500k"
FPS="25"
QUAL="ultrafast"

PASSWORD="7096572120"

SOURCE="http://10.5.5.9:8080/live/amba.m3u8"

YOUTUBE_URL="rtmp://a.rtmp.youtube.com/live2"
YOUTUBE_KEY="zegf-pc3c-6bzb-cs55"

echo "Enable Preview..."
curl http://10.5.5.9/camera/PV?t=${PASSWORD}&p=%02

echo "Start Streaming..."
ffmpeg \
    -thread_queue_size 1024 -probesize 8192 -i "$SOURCE" -deinterlace \
    -vcodec libx264 -pix_fmt yuv420p -preset ${QUAL} -r ${FPS} -g $(($FPS * 2)) -b:v ${VBR} \
    -acodec libmp3lame -ar 11025 -b:a 16k -af "volume=0" -threads 6 -bufsize 3968k \
    -tune zerolatency -maxrate 1984k \
    -flags +global_header -f flv "$YOUTUBE_URL/$YOUTUBE_KEY"
